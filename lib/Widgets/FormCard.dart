import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:juego/model/loginModel.dart';
import 'package:juego/model/userModel.dart';
import 'package:juego/services/login.dart';
import 'package:juego/services/usuario.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FormCard extends StatelessWidget {
  final _formRegisterKey = GlobalKey<FormState>();

  final rNombreInputController = TextEditingController();
  final rApellidoInputController = TextEditingController();
  final rCorreoInputController = TextEditingController();
  final rPasswordInputController = TextEditingController();

  @override
  void dispose() {
    rNombreInputController.dispose();
    rApellidoInputController.dispose();
    rCorreoInputController.dispose();
    rPasswordInputController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: double.infinity,
      height: ScreenUtil.getInstance().setHeight(500),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.0),
          boxShadow: [
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0),
            BoxShadow(
                color: Colors.black12,
                offset: Offset(0.0, 15.0),
                blurRadius: 15.0)
          ]),
      child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('Login',
                style: TextStyle(
                    fontSize: ScreenUtil.getInstance().setSp(45),
                    fontFamily: 'Poppins-Bold',
                    letterSpacing: .6)),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text('Username',
                style: TextStyle(
                    fontFamily: 'Poppins-Bold',
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextField(
              onChanged: (text){
                _registerLoginInfo(text, 'username');
              },
              decoration: InputDecoration(
                  hintText: 'username',
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(30),
            ),
            Text('Password',
                style: TextStyle(
                    fontFamily: 'Poppins-Bold',
                    fontSize: ScreenUtil.getInstance().setSp(26))),
            TextField(
              onChanged: (text){
                _registerLoginInfo(text, 'password');
              },
              decoration: InputDecoration(
                  hintText: 'Password',
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 12.0)),
                  obscureText: true,
            ),
            SizedBox(
              height: ScreenUtil.getInstance().setHeight(35),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                new GestureDetector(
                  onTap: () {
                    _openRegisterForm(context);
                  },
                  child: new Text(
                    'No estas registrado?, Registrarme.',
                    style: TextStyle(
                        color: Colors.blueGrey,
                        fontFamily: 'Poppins-Bold',
                        fontSize: ScreenUtil.getInstance().setSp(28)),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  _openRegisterForm(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Registro'),
            content: Form(
                key: _formRegisterKey,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Nombres'),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Este campo es requerido.';
                                }
                              },
                              controller: rNombreInputController)),
                      Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                              decoration:
                                  InputDecoration(hintText: 'Apellidos'),
                              keyboardType: TextInputType.text,
                              textInputAction: TextInputAction.next,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Este campo es requerido.';
                                }
                              },
                              controller: rApellidoInputController)),
                      Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                              decoration: InputDecoration(hintText: 'Correo'),
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Este campo es requerido.';
                                }
                              },
                              controller: rCorreoInputController)),
                      Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                              decoration:
                                  InputDecoration(hintText: 'Contraseña'),
                              textInputAction: TextInputAction.done,
                              obscureText: true,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Este campo es requerido.';
                                }
                              },
                              controller: rPasswordInputController)),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: RaisedButton(
                          child: Text("Registrarme"),
                          onPressed: () {
                            if (_formRegisterKey.currentState.validate()) {
                              _registerUser(context);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                )),
          );
        });
  }

  _registerLoginInfo(String text, String type) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(type, text);
  }

  _registerUser(BuildContext context) async {
    var userModel = UserModel(nombres: rNombreInputController.text,
                              apellidos: rApellidoInputController.text,
                              username: rCorreoInputController.text,
                              password: rPasswordInputController.text);

    await UserService().postUser(userModel);

    Navigator.of(context).pop();
  }
}
