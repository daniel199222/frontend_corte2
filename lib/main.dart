import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:juego/model/loginModel.dart';
import 'package:juego/pages/categoria.dart';
import 'package:juego/pages/play.dart';
import 'package:juego/pages/profile.dart';
import 'package:juego/pages/ranking.dart';
import 'package:juego/pages/selection.dart';
import 'package:juego/pages/info.dart';
import 'package:juego/services/login.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Widgets/FormCard.dart';

void main() => runApp(MaterialApp(
        home: MyApp(),
        debugShowCheckedModeBanner: false,
        routes: <String, WidgetBuilder>{

          Ranking.routeName: (BuildContext context) => Ranking(),
          Selection.routeName: (BuildContext context) => Selection(),
          Categoria.routeName: (BuildContext context) => Categoria(),
          Profile.routeName: (BuildContext context) => Profile(),
          Info.routeName: (BuildContext context) => Info(),
          //Play.routeName: (BuildContext context) => Play(1),
        }));

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isSelected = false;
  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 750, height: 1334, allowFontScaling: true);
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Image.asset('assets/images/imagen.PNG')),
            ],
          ),
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/icon.PNG',
                        width: ScreenUtil.getInstance().setWidth(110),
                        height: ScreenUtil.getInstance().setHeight(110),
                      ),
                      Text('SabeloTodo',
                          style: TextStyle(
                              fontFamily: 'Poppins-Bold',
                              fontSize: ScreenUtil.getInstance().setSp(46),
                              letterSpacing: .6,
                              fontWeight: FontWeight.bold))
                    ],
                  ),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(180)),
                  FormCard(),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(40)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          SizedBox(
                            width: 12.0,
                          ),
                          GestureDetector(
                            onTap: _radio,
                            child: radioButton(_isSelected),
                          ),
                          SizedBox(
                            width: 8.0,
                          ),
                          Text('Recordarme',
                              style: TextStyle(
                                  fontSize: 12, fontFamily: 'Poppins-Medium'))
                        ],
                      ),
                      InkWell(
                        child: Container(
                          width: ScreenUtil.getInstance().setWidth(330),
                          height: ScreenUtil.getInstance().setHeight(100),
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                                Color(0xFF17ead9),
                                Color(0xFF6078ea)
                              ]),
                              borderRadius: BorderRadius.circular(6.0),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFF6078ea).withOpacity(.3),
                                    offset: Offset(0.0, 8.0),
                                    blurRadius: 8.0)
                              ]),
                          child: Material(
                            color: Colors.transparent,
                            child: InkWell(
                              child: Center(
                                child: Text('Sigin',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Poppins-Bold',
                                        fontSize: 18,
                                        letterSpacing: 1.0)),
                              ),
                              onTap: () {
                                _validateUser(context);
                              },
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  _validateUser(BuildContext context) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var loginModel = LoginModel(username: prefs.getString('username'), password: prefs.getString('password'));
    await LoginService().doLogin(loginModel);
    
    await Navigator.push(context, MaterialPageRoute(builder: (context) => Selection()));
  }
} 
