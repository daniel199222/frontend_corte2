import 'dart:async';
import 'dart:convert' show json;
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HttpHandler {
  final String _baseUrl = '35.239.94.69:3000';
  //final String _baseUrl = '192.168.0.28:3000';
  final String _tokenKeyword = '';

  Future<http.Response> postLogin(objBody) {
    var uri = Uri.http(_baseUrl, 'login');
    return http.post(uri,
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encoder.convert(objBody));
  }

  Future<http.Response> postUser(objBody) async {
    var uri = Uri.http(_baseUrl, 'sign-up');
    return http.post(uri,
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encoder.convert(objBody));
  }

  Future<http.Response> getJson(service) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = _tokenKeyword + prefs.getString('token');
    String username = prefs.getString('username');

    var uri = Uri.http(_baseUrl, service);
    return http.get(uri, headers: {
      HttpHeaders.authorizationHeader: token,
      'username': username
    });
  }

  Future<http.Response> postJson(service, objBody) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = _tokenKeyword + prefs.getString('token');
    String username = prefs.getString('username');
    var requestBody = objBody;
    // requestBody.idusuario = idUsuario;

    var uri = Uri.http(_baseUrl, service);
    return http.post(uri,
        headers: {
          HttpHeaders.authorizationHeader: token,
          HttpHeaders.contentTypeHeader: 'application/json',
          'username': username
        },
        body: json.encoder.convert(requestBody));
  }

  Future<http.Response> putJson(service, objBody) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = _tokenKeyword + prefs.getString('token');
    String username = prefs.getString('username');
    var requestBody = objBody;
    // requestBody.idusuario = idUsuario;

    var uri = Uri.http(_baseUrl, service);
    return http.post(uri,
        headers: {
          HttpHeaders.authorizationHeader: token,
          HttpHeaders.contentTypeHeader: 'application/json',
          'username': username
        },
        body: json.encoder.convert(requestBody));
  }

  Future<http.Response> deleteJson(service, codigo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = _tokenKeyword + prefs.getString('token');
    String username = prefs.getString('username');
    var completeService = service + '/' + codigo.toString();

    var uri = Uri.http(_baseUrl, completeService);
    return http.delete(uri, headers: {
      HttpHeaders.authorizationHeader: token,
      'username': username
    });
  }
}
