import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:juego/model/preguntaModel.dart';
import 'package:juego/model/rankingModel.dart';
import 'package:juego/services/preguntas.dart';
import 'package:juego/services/ranking.dart';

class Play extends StatefulWidget {
  static const String routeName = '/play';
  final int _idCategoria;
  const Play(this._idCategoria);
  @override
  _Play createState() => _Play(_idCategoria);
}

class _Play extends State<Play> {
  final int _idCategoria;
  _Play(this._idCategoria);
  List<PreguntaModel> listPregunta = List<PreguntaModel>();

  /*
  _buildList(){
    return <PreguntaModel>[
      PreguntaModel(idPregunta:1,quest:'Cual no es ciudad de colombia',respA: 'perro',respB: 'Cali',respC: 'Bogota',respD: 'Medellin',respCorre: 'perro'),
      PreguntaModel(idPregunta:2,quest:'Como se llama la estrella mas grande de nuestro sistema solar',respA: 'Sol',respB: 'Luna',respC: 'Venus',respD: 'Marte',respCorre: 'Sol'),
      PreguntaModel(idPregunta:3,quest:'Rio Pance donde queda',respA: 'Cali',respB: 'Medellin',respC: 'Bogota',respD: 'Palmira',respCorre: 'Cali'),
      PreguntaModel(idPregunta:4,quest:'El rio cauca no pasa por el departamento',respA: 'Valle Del Cauca',respB: 'Antioquia',respC: 'Arauca',respD: 'Cauca',respCorre: 'Arauca'),
      PreguntaModel(idPregunta:5,quest:'Capital de colombia',respA: 'Cali',respB: 'Barranquilla',respC: 'Medellin',respD: 'Bogota',respCorre: 'Bogota'),
      PreguntaModel(idPregunta:6,quest:'Que ciudad no pertenece al oceano atlantico',respA: 'Buenaventura',respB: 'Cartagena',respC: 'Santa Marca',respD: 'San andres',respCorre: 'Buenaventura'),
      PreguntaModel(idPregunta:7,quest:'Cual es el pais mas pequeño',respA: 'Andorra',respB: 'Ciudad de Vaticano',respC: 'Bolivia',respD: 'Francia',respCorre: 'Ciudad de Vaticano'),
      PreguntaModel(idPregunta:8,quest:'Hawaii pertenece a',respA: 'Estados unidos',respB: 'Canada',respC: 'España',respD: 'Italia',respCorre: 'Estados unidos'),
      PreguntaModel(idPregunta:9,quest:'Que pais esta mas cerca a San Andres Islas',respA: 'Colombia',respB: 'Mexico',respC: 'Puerto Rico',respD: 'Nicaragua',respCorre: 'Nicaragua'),
      PreguntaModel(idPregunta:10,quest:'Que pais tiene la inflacion mas alta',respA: 'Colombia',respB: 'Irak',respC: 'Siria',respD: 'Venezuela',respCorre: 'Venezuela')
    ];
  }*/

  @override
  void initState() {
    super.initState();
    _getPregunta();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('SABELO TODO')),
      body: PreguntaJuego(listPregunta,_idCategoria),
    );
  }

  _getPregunta() async {
    List<PreguntaModel> tmpLists =
        await PreguntasService().getPregunta(_idCategoria);
    setState(() {
      listPregunta = tmpLists;
    });
  }
}

class PreguntaJuego extends StatelessWidget {
  final List<PreguntaModel> _pregunta;
  final int idCaegoria;
  const PreguntaJuego(this._pregunta,this.idCaegoria);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Center(
                child: Column(
      children: _buildListPregunta(),
    ))));
  }

  List<ItemPregunta> _buildListPregunta() {
    return _pregunta.map((pregunta) => ItemPregunta(pregunta,idCaegoria)).toList();
  }
}

class ItemPregunta extends StatelessWidget {
  final PreguntaModel _pregunta;
  final int idCaegoria;
  const ItemPregunta(this._pregunta,this.idCaegoria);
  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: CircleAvatar(child: Icon(Icons.credit_card)),
        title: Text(_pregunta.descripcion),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ScreenPregunta(_pregunta,idCaegoria)));
        });
  }
}

class ScreenPregunta extends StatelessWidget {
  final PreguntaModel _pregunta;
  final int idCaegoria;
  const ScreenPregunta(this._pregunta,this.idCaegoria);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Pregunta #' + _pregunta.id.toString())),
        body: Container(
            child: Center(
                child: Column(children: <Widget>[
          Text(_pregunta.descripcion,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontFamily: 'Poppins-Bold',
                  fontSize: ScreenUtil.getInstance().setSp(60),
                  letterSpacing: .6,
                  fontWeight: FontWeight.bold)),
          MaterialButton(
            color: Colors.greenAccent,
            child: Column(children: <Widget>[
              //Icon(Icons.looks_one),
              Text(_pregunta.respuesta1,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold))
            ]),
            onPressed: () {
              _status(_pregunta.respuesta_correcta, _pregunta.respuesta1);
              Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Play(idCaegoria)));
            },
          ),
          MaterialButton(
            color: Colors.orangeAccent,
            child: Column(children: <Widget>[
              //Icon(Icons.looks_two),
              Text(_pregunta.respuesta2,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold))
            ]),
            onPressed: () {
              _status(_pregunta.respuesta_correcta, _pregunta.respuesta2);
              Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Play(idCaegoria)));
            },
          ),
          MaterialButton(
            color: Colors.blueAccent,
            child: Column(children: <Widget>[
              //Icon(Icons.looks_3),
              Text(_pregunta.respuesta3,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold))
            ]),
            onPressed: () {
              _status(_pregunta.respuesta_correcta, _pregunta.respuesta3);
              Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Play(idCaegoria)));
            },
          ),
          MaterialButton(
            color: Colors.redAccent,
            child: Column(children: <Widget>[
              //Icon(Icons.looks_4),
              Text(_pregunta.respuesta4,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold))
            ]),
            onPressed: () {
              _status(_pregunta.respuesta_correcta, _pregunta.respuesta4);
              Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Play(idCaegoria)));
            },
          ),
        ]))));
  }

  _status(pregunta, respuesta) {
    List<RankingModel> ranking = List<RankingModel>();
    if (pregunta == respuesta) {
      print('Es correcta');
      _postRanking(_pregunta.id,1);
      Fluttertoast.showToast(
          msg: 'La respuesta es correcta.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.blueGrey,
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      print('Es incorrecta');
      _postRanking(_pregunta.id,0);
      Fluttertoast.showToast(
          msg: 'La respuesta incorrecta.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.blueGrey,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  _postRanking(int idP, int idR) async {
    var rankingModel = RankingModel(
        id: 0,
        idPregunta: idP,
        idRespuesta: idR,
        idUsuario: 0);
    await RankingService().postRanking(rankingModel);
  }
}
