import 'package:flutter/material.dart';
import 'package:juego/pages/categoria.dart';

class Selection extends StatefulWidget {
  static const String routeName = '/selection';
  @override
  _SelectionState createState() => new _SelectionState();
}

class _SelectionState extends State<Selection> {
  Drawer _getDrawer(BuildContext context) {
    ListTile _getItem(Icon icon, String description, String route) {
      return new ListTile(
          leading: icon,
          title: new Text(description),
          onTap: () {
            setState(() {
              Navigator.of(context).pushNamed(route);
            });
          });
    }

    ListView listView = new ListView(children: <Widget>[
      _getItem(new Icon(Icons.home), 'Inicio', '/selection'),
      _getItem(new Icon(Icons.people_outline), 'Perfil', '/profile'),
      _getItem(new Icon(Icons.people), 'Categoria', '/categoria'),
      //_getItem(new Icon(Icons.play_arrow), 'Juego', '/play'),
      _getItem(new Icon(Icons.accessibility_new), 'Clasificacion', '/ranking'),
      _getItem(new Icon(Icons.error_outline), 'Acerca de', '/info'),
    ]);
    return new Drawer(
      child: listView,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('BIENVENIDOS'),
      ),
      body: new Container(
        child: new Center(
          child: new Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "",
                    style: new TextStyle(
                      fontSize: 60.0,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.all(7.0),
                  ),
                  new Text(
                    "BIENVENIDOS",
                    style: new TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "AL JUEGO",
                    style: new TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "SABELOTODO",
                    style: new TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "",
                    style: new TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                ],
              ),
              new Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                        top: 4.0,
                        left: 80.0,
                        right: 0.0,
                      ),
                      child: Image.asset('assets/images/imagen.PNG')),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "",
                    style: new TextStyle(
                      fontSize: 70.0,
                    ),
                  ),
                ],
              ),
              new RaisedButton(
                disabledColor: Colors.blueAccent,
                child: Text('INICIAR',
                    style: new TextStyle(fontSize: 25.0, color: Colors.white)),
                splashColor: Colors.amber,
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) => new Categoria()));
                },
              ),
            ],
          ),
        ),
      ),
      drawer: _getDrawer(context),
    );
  }
}
