import 'package:flutter/material.dart';
import 'package:juego/model/categoriaModel.dart';
import 'package:juego/pages/play.dart';
import 'package:juego/services/categoria.dart';

class Categoria extends StatefulWidget {
  static const String routeName = '/categoria';

  @override
  _Categoria createState() => _Categoria();
}

class _Categoria extends State<Categoria> {
  List<CategoriaModel> listCategorias = List<CategoriaModel>();

  @override
  void initState() {
    super.initState();
    _getCategorias();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ESCOGE LA CATEGORIA')),
      body: ListCategoria(listCategorias),
    );
  }

  _getCategorias() async {
    List<CategoriaModel> tmpLists = await CategoriaService().getCategorias();
    setState(() {
      listCategorias = tmpLists;
    });
  }
}

class ListCategoria extends StatelessWidget {
  final List<CategoriaModel> _categoria;
  const ListCategoria(this._categoria);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _buildListCategoria(),
    );
  }

  List<ItemCategoria> _buildListCategoria() {
    return _categoria.map((categoria) => ItemCategoria(categoria)).toList();
  }
}

class ItemCategoria extends StatelessWidget {
  //instancia para identificar el modelo
  final CategoriaModel _categoria;
  const ItemCategoria(this._categoria);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: CircleAvatar(child: Icon(Icons.blur_on)),
        title: Text(_categoria.descripcion),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
            builder: (context) => Play(_categoria.id)));
        });
  }
}