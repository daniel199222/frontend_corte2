import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:juego/model/userModel.dart';
import 'package:juego/services/usuario.dart';

class Profile extends StatefulWidget {
  static const String routeName = '/profile';

  @override
  _ProfileState createState() => new _ProfileState();
}

class _ProfileState extends State<Profile> {
  String nombre = '';
  String apellido = '';
  String usuario = '';
  String position = '';

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PERFIL'),
      ),
      body: Container(
            child: Column(
                //cuerpo del aplicativo para crecion del item
                children: <Widget>[
              new Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(
                        top: 3.0,
                        left: 60.0,
                        right: 0.0,
                      ),
                      child: Image.asset('assets/images/imagen.PNG')),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "",
                    style: new TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "PERFIL",
                    style: new TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Text(
                    "",
                    style: new TextStyle(
                      fontSize: 20.0,
                    ),
                  ),
                ],
              ),
              Text('Nombre: ' + nombre,
                 textAlign: TextAlign.left,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold)),
              Padding(
                padding: EdgeInsets.all(8.0),
              ),
              Text('Apellido: ' + apellido,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold)),
              Padding(
                padding: EdgeInsets.all(8.0),
              ),
              Text('Usuario: ' + usuario,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold)),
              Padding(
                padding: EdgeInsets.all(8.0),
              ),
              Text('Posición: ' + position,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Poppins-Bold',
                      fontSize: ScreenUtil.getInstance().setSp(40),
                      letterSpacing: .6,
                      fontWeight: FontWeight.bold))
            ]),
      ),
    );
  }

  _getUser() async {
    UserModel userTmp = await UserService().getUser();

    setState(() {
      nombre = userTmp.nombres;
      apellido = userTmp.apellidos;
      usuario = userTmp.username;
      position = userTmp.position;
    });
  }
}
