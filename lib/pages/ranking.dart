import 'package:flutter/material.dart';
import 'package:juego/model/rankingModel.dart';
import 'package:juego/services/ranking.dart';

class Ranking extends StatefulWidget {
  static const String routeName = '/ranking';
  @override
  _Ranking createState() => new _Ranking();
}

class _Ranking extends State<Ranking> {

  List<RankingModel> listRanking = new List<RankingModel>();
  /*
  _buildList(){
    return <RankingModel>[
      RankingModel(idRanking:1,nombre:'Daniel Calvo',nickname: 'Daniel199222',puntaje: 20),
      RankingModel(idRanking:2,nombre:'Deymer Jimenez',nickname: 'DeymerJ',puntaje: 18),
      RankingModel(idRanking:1,nombre:'Karen Sanchez',nickname: 'sanchezK',puntaje: 17),
      RankingModel(idRanking:1,nombre:'Monica Perez',nickname: 'MoniPe',puntaje: 16),
      RankingModel(idRanking:1,nombre:'Ricardo Cordoba',nickname: 'Rcordoba',puntaje: 10),
      RankingModel(idRanking:1,nombre:'Jhon Ramirez',nickname: 'Loco',puntaje: 9)
    ];
  }*/

    @override
  void initState() {
    super.initState();
    _getRanking();
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Ranking')),
      body: new ListRanking(listRanking),

    );
  }

  _getRanking() async {
    List<RankingModel> tmpLists =
        await RankingService().getRanking();
    setState(() {
      listRanking = tmpLists;
    });
  }
}

class ListRanking extends StatelessWidget {
  final List<RankingModel> _ranking;
  const ListRanking(this._ranking);
  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: _buildListRanking(),
    );
  }

  List<ItemRanking> _buildListRanking() {
    return _ranking
        .map((ranking) => new ItemRanking(ranking))
        .toList();
  }
}

class ItemRanking extends StatelessWidget {
  //instancia para identificar el modelo
  final RankingModel _ranking;
  const ItemRanking(this._ranking);
  @override
  Widget build(BuildContext context) {
    return new ListTile(
        leading: new CircleAvatar(child: new Icon(Icons.assignment)),
        title: new Text(_ranking.id.toString()),
        subtitle: new Text('Usuario: ID '+_ranking.idUsuario.toString() +' Respuestas : '+ _ranking.idRespuesta.toString()),
    );
  }
}