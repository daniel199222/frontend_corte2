import 'package:json_annotation/json_annotation.dart';

part 'loginModel.g.dart';

@JsonSerializable(includeIfNull: false)

class LoginModel {
  
  @JsonKey(nullable: false)
  final String username;

  @JsonKey(nullable: false)
  final String password;

  @JsonKey(name: 'jwtToken', nullable: true)
  final String token;

  LoginModel({this.username, this.password, this.token});

  factory LoginModel.fromJson(Map<String, dynamic> json) => _$LoginModelFromJson(json);
  Map<String, dynamic> toJson() => _$LoginModelToJson(this);
}