import 'package:json_annotation/json_annotation.dart';

@JsonSerializable(includeIfNull: false)
class CategoriaModel {
  final int id;
  final String descripcion;

  CategoriaModel({this.id, this.descripcion});

  factory CategoriaModel.fromJson(Map<String, dynamic> json) {
    return CategoriaModel(
      id: json['id'] as int,
      descripcion: json['descripcion'] as String,
    );
  }
  Map<String, dynamic> toJson(CategoriaModel instance) {
    final val = <String, dynamic>{};

    void writeNotNull(String key, dynamic value) {
      if (value != null) {
        val[key] = value;
      }
    }

    writeNotNull('id', instance.id);
    writeNotNull('descripcion', instance.descripcion);
    return val;
  }
}
