import 'package:json_annotation/json_annotation.dart';

part 'rankingModel.g.dart';

@JsonSerializable(includeIfNull: false)

class RankingModel{
  final int id;
  final int idPregunta;
  final int idRespuesta;
  final int idUsuario;

  RankingModel({this.id,this.idPregunta,this.idRespuesta,this.idUsuario});

  factory RankingModel.fromJson(Map<String, dynamic> json) => _$RankingModelFromJson(json);
  Map<String, dynamic> toJson() => _$RankingModelToJson(this);
  
}