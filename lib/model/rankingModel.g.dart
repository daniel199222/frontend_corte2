// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rankingModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RankingModel _$RankingModelFromJson(Map<String, dynamic> json) {
  return RankingModel(
    id: json['idRanking'] as int,
    idPregunta: json['idPregunta'] as int,
    idRespuesta: json['idRespuesta'] as int,
    idUsuario: (json['idUsuario'] as num)?.toInt(),
  );
}

Map<String, dynamic> _$RankingModelToJson(RankingModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('idPregunta', instance.idPregunta);
  writeNotNull('idRespuesta', instance.idRespuesta);
  writeNotNull('idUsuario', instance.idUsuario);
  return val;
}
