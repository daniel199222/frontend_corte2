import 'package:json_annotation/json_annotation.dart';

part 'preguntaModel.g.dart';

@JsonSerializable(includeIfNull: false)

class PreguntaModel{
  final int id;
  final String descripcion;
  final String respuesta1;
  final String respuesta2;
  final String respuesta3;
  final String respuesta4;
  final String respuesta_correcta;
  final int id_categoria;

  PreguntaModel({this.id, this.descripcion, this.respuesta1, this.respuesta2, this.respuesta3, this.respuesta4, this.respuesta_correcta,this.id_categoria});

  factory PreguntaModel.fromJson(Map<String, dynamic> json) => _$PreguntaModelFromJson(json);
  Map<String, dynamic> toJson() => _$PreguntaModelToJson(this);
  
}