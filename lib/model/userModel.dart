import 'package:json_annotation/json_annotation.dart';

part 'userModel.g.dart';

@JsonSerializable(includeIfNull: false)

class UserModel {
  
  @JsonKey(nullable: false)
  final String nombres;

  @JsonKey(nullable: false)
  final String apellidos;

  @JsonKey(nullable: false)
  final String username;

  @JsonKey(nullable: false)
  final String password;

  @JsonKey(nullable: true)
  final String position;

  UserModel({this.nombres, this.apellidos, this.username, this.password, this.position});

  factory UserModel.fromJson(Map<String, dynamic> json) => _$UserModelFromJson(json);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}