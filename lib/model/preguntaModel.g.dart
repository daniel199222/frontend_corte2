// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'preguntaModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PreguntaModel _$PreguntaModelFromJson(Map<String, dynamic> json) {
  return PreguntaModel(
    id: json['id'] as int,
    descripcion: json['descripcion'] as String,
    respuesta1: json['respuesta1'] as String,
    respuesta2: json['respuesta2'] as String,
    respuesta3: json['respuesta3'] as String,
    respuesta4: json['respuesta4'] as String,
    respuesta_correcta: json['respuesta_correcta'] as String,
    id_categoria: json['id_categoria'] as int,
  );
}

Map<String, dynamic> _$PreguntaModelToJson(PreguntaModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('descripcion', instance.descripcion);
  writeNotNull('respuesta1', instance.respuesta1);
  writeNotNull('respuesta2', instance.respuesta2);
  writeNotNull('respuesta3', instance.respuesta3);
  writeNotNull('respuesta4', instance.respuesta4);
  writeNotNull('respuesta_correcta', instance.respuesta_correcta);
  writeNotNull('id_categoria', instance.id_categoria);
  return val;
}
