import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:juego/common/HttpHandler.dart';
import 'package:juego/model/loginModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginService {
  doLogin(LoginModel loginModel) async {
    final response = await HttpHandler().postLogin(loginModel.toJson());

    if (response.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString(
          'token', response.headers[HttpHeaders.authorizationHeader]);
    } else {
      await Fluttertoast.showToast(
          msg: 'Usuario y/o contraseña incorrecctos.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

      throw Exception('Usuario y/o contraseña incorrecctos.');
    }
  }
}
