import 'dart:convert';
import 'package:juego/common/HttpHandler.dart';
import 'package:juego/model/categoriaModel.dart';


class CategoriaService {

  final String servicio = 'api/V1/categoria';

  Future<List> getCategorias() async {
      var request = servicio;
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);
      print('esta es la respuesta '+response.body);

      var list = data as List;
      List<CategoriaModel> cardsList = list.map((i) => CategoriaModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la Categoria.');
      }
  }
  Future<String> postCategoria(CategoriaModel CategoriaModel) async {
      final response = await HttpHandler().postUser(CategoriaModel);
      if (response.statusCode == 200) {
        return 'Categoria creado';
      } else {
        throw Exception(response.body);
      }
    }

   Future<String> putCategoria(CategoriaModel CategoriaModel) async {
      final response = await HttpHandler().putJson(servicio, CategoriaModel);
      if (response.statusCode == 200) {
        return 'Categoria Actualizado';
      } else {
        throw Exception(response.body);
      }
    }
    Future<String> deleteCategoria(int idCategoria) async {
      final response = await HttpHandler().deleteJson(servicio, idCategoria);
      if (response.statusCode == 200) {
        return 'Categoria eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
