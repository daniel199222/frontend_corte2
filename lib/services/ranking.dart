import 'dart:convert';
import 'package:juego/common/HttpHandler.dart';
import 'package:juego/model/rankingModel.dart';


class RankingService {

  final String servicio = 'api/V1/resultados';

  Future<List> getRanking() async {
      var request = servicio;
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);
      print('esta es la ranking '+response.body);

      var list = data as List;
      List<RankingModel> cardsList = list.map((i) => RankingModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la ranking.');
      }
  }
  Future<List> getPregunta(int idCategoria) async {
      var request = servicio+'/'+idCategoria.toString();
      print('esta es la el id que llego '+idCategoria.toString());
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);
      print('esta es la respuesta '+response.body);

      var list = data as List;
      List<RankingModel> cardsList = list.map((i) => RankingModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la ranking.');
      }
  }

  Future<String> postRanking(RankingModel RankingModel) async {
      final response = await HttpHandler().postUser(RankingModel);
      if (response.statusCode == 200) {
        return 'ranking creado';
      } else {
        throw Exception(response.body);
      }
    }

   Future<String> putRanking(RankingModel RankingModel) async {
      final response = await HttpHandler().putJson(servicio, RankingModel);
      if (response.statusCode == 200) {
        return 'ranking Actualizado';
      } else {
        throw Exception(response.body);
      }
    }
    Future<String> deleteRanking(int idTarjeta) async {
      final response = await HttpHandler().deleteJson(servicio, idTarjeta);
      if (response.statusCode == 200) {
        return 'ranking eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
