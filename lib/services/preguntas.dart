import 'dart:convert';
import 'package:juego/common/HttpHandler.dart';
import 'package:juego/model/preguntaModel.dart';


class PreguntasService {

  final String servicio = 'api/V1/preguntas';

  Future<List> getPreguntas() async {
      var request = servicio;
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);
      print("esta es la respuesta "+response.body);

      var list = data as List;
      List<PreguntaModel> cardsList = list.map((i) => PreguntaModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la Pregunta.');
      }
  }
  Future<List> getPregunta(int idCategoria) async {
      var request = servicio+'/'+idCategoria.toString();
      print('esta es la el id que llego '+idCategoria.toString());
      final response = await HttpHandler().getJson(request);
      var data = json.decode(response.body);
      print('esta es la respuesta '+response.body);

      var list = data as List;
      List<PreguntaModel> cardsList = list.map((i) => PreguntaModel.fromJson(i)).toList();

      if (response.statusCode == 200) {        
        return cardsList;
      } else {
        throw Exception('No se encontro la informacion de la Pregunta.');
      }
  }

  Future<String> postTarjeta(PreguntaModel PreguntaModel) async {
      final response = await HttpHandler().postUser(PreguntaModel);
      if (response.statusCode == 200) {
        return 'Pregunta creado';
      } else {
        throw Exception(response.body);
      }
    }

   Future<String> putTarjeta(PreguntaModel PreguntaModel) async {
      final response = await HttpHandler().putJson(servicio, PreguntaModel);
      if (response.statusCode == 200) {
        return 'Pregunta Actualizado';
      } else {
        throw Exception(response.body);
      }
    }
    Future<String> deleteTarjeta(int idTarjeta) async {
      final response = await HttpHandler().deleteJson(servicio, idTarjeta);
      if (response.statusCode == 200) {
        return 'Pregunta eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
