import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:juego/common/HttpHandler.dart';
import 'package:juego/model/userModel.dart';

class UserService {

  final String servicio = 'api/usuario';
    
    Future<UserModel> getUser() async {
      
      String request = servicio;

      final response = await HttpHandler().getJson(request);
      print(response.body);
      if (response.statusCode == 200) {
        return UserModel.fromJson(json.decode(response.body));
      } else {
        await Fluttertoast.showToast(
          msg: 'No se encontro la informacion del usuario.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);

        throw Exception('No se encontro la informacion del usuario.');
      }
    }

    postUser(UserModel userModel) async {
      final response = await HttpHandler().postUser(userModel);
      if (response.statusCode == 200) {
        await Fluttertoast.showToast(
          msg: 'Usuario creado.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.green,
          textColor: Colors.black,
          fontSize: 16.0);
      } else {
        await Fluttertoast.showToast(
          msg: 'Error al crear el usuario.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
          
        throw Exception(response.body);
      }
    }

    Future<String> putUser(UserModel userModel) async {
      final response = await HttpHandler().putJson(servicio, userModel);
      if (response.statusCode == 200) {
        return 'Usuario Actualizado';
      } else {
        throw Exception(response.body);
      }
    }

    Future<String> deleteUser(int idUsuario) async {
      final response = await HttpHandler().deleteJson(servicio, idUsuario);
      if (response.statusCode == 200) {
        return 'Usuario eliminado';
      } else {
        throw Exception(response.body);
      }
    }
}
